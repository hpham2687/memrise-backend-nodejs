const express = require('express');
const router = express.Router();

var noteController = require('../controllers/noteController');


router.route('/get')
    .get(noteController.getAllNote);



module.exports = router;