const express = require('express');
const router = express.Router();

var userController = require('../controllers/userController');


router.route('/create')
    .post(userController.middlewareCreateUser, userController.createUser)
    .get(() => { console.log('get') });


router.route('/update/:id')
    .post(userController.updateUser);

router.route('/update/listword/:id')
    .post(userController.addToListWord);


router.route('/delete/:id')
    .get(userController.deleteUser);




router.route('/get')
    .get(userController.getAllUser);

router.route('/get/:id')
    .get(userController.getUser);




module.exports = router;