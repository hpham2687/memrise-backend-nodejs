const mongoose = require('mongoose');



//db connect
const DB = process.env.DATABASE.replace('<PASSWORD>', process.env.DB_PASSWORD).replace('<DB_NAME>', process.env.DB_NAME); // connect to memrise document (config in url)
function connect() {
    try {
        mongoose.connect(DB, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false
        }).then(connection => {
            console.log('db connected!');

        })
    } catch (error) {
        handleError(error);
    }
}

module.exports = {
    connect: connect
}