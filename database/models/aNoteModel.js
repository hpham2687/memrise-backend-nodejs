const mongoose = require('mongoose');
var ANotesSchema = new mongoose.Schema({
    noteTitle: String,
    noteContent: String,
    created_date: Date
});

const ANote = mongoose.model('ANote', ANotesSchema); // connecnt to connecttion

module.exports = ANote;