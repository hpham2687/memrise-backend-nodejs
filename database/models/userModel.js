const mongoose = require('mongoose');
var AWordSchema = new mongoose.Schema({
    word: String,
    definition: String,
    pronunciation: String,
    learn_date: Date
});
const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, 'username must be not emtpy'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'password must be not emtpy']
    },
    email: {
        type: String,
        required: [true, 'email must be not emtpy'],
        unique: true
    },
    role: {
        type: Number,
        default: 2
    },
    list_word: {
        type: [AWordSchema],
        default: { word: "word", definition: "definition", pronunciation: "pronunciation", leared_date: Date.now() }
    }
});
const Users = mongoose.model('Users', userSchema); // connecnt to connecttion

module.exports = Users;