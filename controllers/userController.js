const User = require('./../database/models/userModel')


exports.deleteUser = async (req, res) => {

    try {
        await User.findByIdAndDelete(req.params.id);
        res.status(200).json({
            status: 'success',
            msg: 'xoa thanh cong ban ei'
        })

    } catch (err) {
        res.status(400).json({
            status: 'fail',
            msg: err
        })
    }

}

exports.addToListWord = async (req, res) => {

    try {
        var user = await User.findOne({ _id: req.params.id });
        var data2add = req.body;


        var updatedListWord = await User.updateOne({ _id: req.params.id }, { $push: { list_word: data2add } });



        res.status(200).json({
            status: 'success',
            msg: 'update thanh cong ban ei',
            data: updatedListWord
        })

    } catch (err) {
        res.status(400).json({
            status: 'fail',
            msg: err
        })
    }

}

exports.updateUser = async (req, res) => {

    try {
        var user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true, strict: false }); // new : true to send back the modified data to client, strict false to overwrite field which in the post but not in data


        res.status(200).json({
            status: 'success',
            msg: 'update thanh cong ban ei',
            data: user
        })

    } catch (err) {
        res.status(400).json({
            status: 'fail',
            msg: err
        })
    }

}

exports.getUser = async (req, res) => {

    try {
        var user = await User.findById(req.params.id);
        res.status(200).json({
            status: 'success',
            msg: 'get thanh cong ban ei',
            data: user
        })

    } catch (err) {
        res.status(400).json({
            status: 'fail',
            msg: 'loi chua xac dih'
        })
    }

}
exports.getAllUser = async (req, res) => {

    try {
        const queryObj = { ...req.query };
        const excludedFields = ['page', 'limit'];
        excludedFields.forEach(el => delete queryObj[el]);
        console.log(queryObj);


        var allUser = await User.find(queryObj);
        res.status(200).json({
            status: 'success',
            msg: 'get thanh cong ban ei',
            data: allUser
        })

    } catch (err) {
        res.status(400).json({
            status: 'fail',
            msg: 'loi chua xac dih'
        })
    }

}

exports.createUser = async (req, res) => {

    try {
        var data2add = req.body;
        //  data2add = Object.assign({ "abc": "xyz" }, data2add)
        const newUser = await User.create(data2add);
        res.status(200).json({
            status: 'success',
            msg: 'thanh cong ban ei',
            data: newUser
        })
    } catch (err) {
        if (err.code == 11000) {
            res.status(400).json({
                status: 'fail',
                msg: 'username or email da ton tai'
            })
        } else {
            res.status(400).json({
                status: 'fail',
                msg: 'loi chua xac dih'
            })
        }

    }

}

exports.userSearch = (req, res) => {
    console.log('user tim kiemss')
}

exports.userSearchPost = (req, res) => {
    console.log('user tim kiemss post')
}
exports.middlewareCreateUser = (req, res, next) => {
    var { username, password, email } = req.body;
    if (username === '' || password === '' || email === '') {
        return res.status(400).json({
            status: 'fail',
            msg: 'input contains empty value'

        })

    }
    //  console.log(req.body);
    next();
}