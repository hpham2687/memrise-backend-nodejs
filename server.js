const dotenv = require('dotenv');
dotenv.config({ path: './config.env' })

const DB = require('./database/index');

//db connect
DB.connect();

var app = require('./index');
const port = 4000 || process.env.PORT;

app.listen(port, () => {
    console.log(`app running on port ${port}`);

})