const path = require("path")
module.exports = {
    development: true,
    prefix: process.env.PREFIX || '!', //prefix của bot
    botName: process.env.BOT_NAME || 'Sumi-Chan', //tên bot

    developer: {
        uid: 100005759707662,
        email: 'krysspham@gmail.com',
        github: 'nguyenoanh'
    },
    database: {
        mongodb: {
            database: process.env.DB_NAME,
            username: process.env.DB_USERNAME,
            password: process.env.DB_PASSWORD,
            host: process.env.DATABASE,
        }
    }
}