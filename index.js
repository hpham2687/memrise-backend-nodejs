const express = require('express');
const app = express();
const ANote = require('./database/models/aNoteModel')


if (process.env.NODE_ENV === "development") {
    console.log('devvv')
}
app.disable('etag');

const userRouter = require('./routers/userRouter')
const adminRouter = require('./routers/adminRouter')
const noteRouter = require('./routers/noteRouter')
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});
app.use(express.json());
// var testNote = new ANote({
//     noteTitle: 'dfdsf',
//     noteContent: 't22est',
//     created_date: '2020-02-01T17:00:00.000+00:00'
// })
// testNote.save().then(doc => { console.log(doc) }).catch(err => {
//     console.log(err)
// })
app.get('/', (req, res) => {
    res.send('hello');

})
app.use('/user', userRouter);
app.use('/admin', adminRouter);
app.use('/note', noteRouter);


app.use(express.static(`${__dirname}\\\public`));

module.exports = app;